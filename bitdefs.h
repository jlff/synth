/* ===========================================================================
	AD1CON1
=========================================================================== */
#define ON		15
#define SSRC2	7
#define SSRC1	6
#define SSRC0	5
#define SAMP	1
#define DONE	0

/* ===========================================================================
	ANALOG INPUTS
=========================================================================== */
#define AN2			2
#define AN3			3
#define AN4			4
#define AN5			5
#define AN8			8
#define ADC_CHANNELS	5
#define KEYBOARD		AN2
#define RELEASE_POT		AN3
#define ATTACK_POT		AN4
#define PW_POT			AN5
#define DETUNE_POT		AN8

/* ===========================================================================
	DIGITAL I/O
=========================================================================== */
#define RG6			6
#define RG7			7
#define RG8			8
#define RG9			9
#define CNPUE_RG6	8
#define CNPUE_RG7	9
#define CNPUE_RG8	10
#define CNPUE_RG9	11
#define BUTTON_PORT	PORTG
#define SAW_BUTTON	RG6
#define SQR_BUTTON	RG7
#define TRI_BUTTON	RG8
#define SIN_BUTTON	RG9

#define PDSEL 1
#define STSEL 0
#define BRGH 3
#define UTXEN 10
#define URXEN 12
