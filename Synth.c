#include <plib.h>
#include <stdint.h>
#include "bitdefs.h"

//Top value for Timer1 to generate an interrupt at 40KHz
#define T1_TICK 		1999

#define ADC_RES 		10
#define DAC_RES 		8
#define PHASE_ACC_RES 	21
#define LOOKUP_RES		8
#define ENVELOPE_RES	8

#define ADC_SHC_TIME	0x1F02

#define KBD_SIZE		32
#define KBD_SAMPLE		4092

//#define SEQUENCE_LEN	16

//Struct containing all parameters controllable by the user
struct Parameters {
	//Tuning words for the main oscillators
	uint16_t tuningWord[2];

	//Flag that indicates whether any key is pressed
	uint8_t gate;

	//Tuning words for the envelope generator
	uint16_t attTuningWord;
	uint16_t relTuningWord;

	//Variable that holds the square wave's pulse width
	uint8_t pwmValue;

	//Variable that holds the detune value of the second oscillator
	int16_t detune;

	//Enumerated type that holds the waveform currently being generated
	enum waveform {
		SINE,
		TRIANGLE,
		SAW,
		SQUARE
	} wave;
};

//Wavetable for a quarter sine wave
volatile static const uint8_t sineTable[65] = {
	0x80,0x83,0x86,0x89,0x8c,0x8f,0x92,0x95,
	0x98,0x9c,0x9f,0xa2,0xa5,0xa8,0xab,0xae,
	0xb0,0xb3,0xb6,0xb9,0xbc,0xbf,0xc1,0xc4,
	0xc7,0xc9,0xcc,0xce,0xd1,0xd3,0xd5,0xd8,
	0xda,0xdc,0xde,0xe0,0xe2,0xe4,0xe6,0xe8,
	0xea,0xeb,0xed,0xef,0xf0,0xf2,0xf3,0xf4,
	0xf6,0xf7,0xf8,0xf9,0xfa,0xfb,0xfb,0xfc,
	0xfd,0xfd,0xfe,0xfe,0xfe,0xff,0xff,0xff,
	0xff
};

//Tuning word values for each note
volatile static const uint16_t notes[32] = {
	9155,9699,10276,10887,11534,12220,12947,13717,
	14532,15396,16312,17282,18310,19398,20552,21774,
	23069,24440,25894,27433,29065,30793,32624,34564,
	36619,38797,41104,43548,46137,48881,51787,54867
};

/*
volatile static const uint16_t sequence[MUSIC_LEN] = {
	21774,24440,36619,0,36619,0,0,
	24400,21774,24400,32624,0,32624,30793,0,0
};
*/

//Phase accumulator and tuning word for the main oscillator

//NOTE: The compiler requires all global variables to be declared as 32-bit
//(even though smaller types would suffice in many cases)

//Flag that indicates the occurrence of the sampling interrupt
volatile uint32_t phaseChange = 0;

//Variable that contains the sample to be written
volatile uint32_t output = 0;

//Variable used for keyboard "debouncing"
volatile uint32_t interruptCounter = 0;

void initTimer(void);
//void initTimer2(void);

void initADC(void);
uint16_t acquireADC(uint8_t channel);

void initParams(struct Parameters *params);
void readParams(struct Parameters *params);
void readKeyboard(struct Parameters *params);

uint32_t calcSample(enum waveform wave, uint32_t phase, uint8_t pwmValue);

int main(void)
{
	struct Parameters params;
	initParams(&params);

	//Phase accumulator for each oscillator
	uint32_t phaseAcc[2] = {};

	//Phase accumulator for the envelope generator
	uint32_t envPhaseAcc = 0;

	//Variable that contains the samples of each oscillator
	uint8_t sample[2] = {};

	//Variable that is used to modulate the amplitude of the output
	uint8_t envelope = 0;

	//Setting all the pins connected to the DAC as outputs
	TRISE = 0x00;

	//Setting all the pins connected to the buttons as inputs
	TRISGSET =	(1 << RG6) | (1 << RG7) | (1 << RG8) | (1 << RG9);
	//Enabling pull-up resistors on the inputs
	CNPUESET =	(1 << CNPUE_RG6) | (1 << CNPUE_RG7) | 
				(1 << CNPUE_RG8) | (1 << CNPUE_RG9);

	//Sine wave enabled by default
	params.wave = SINE;

	initTimer();
	initADC();

	while(1) {
		//Read buttons, potentiometers and the keyboard
		readParams(&params);
		if(phaseChange) {
			phaseChange = 0;
			//Calculate the new samples for each oscillator
			sample[0] = calcSample(params.wave, phaseAcc[0], params.pwmValue);
			sample[1] = calcSample(params.wave, phaseAcc[1], params.pwmValue);
			//Increment the phase accumulators of each oscillator
			//and re-adjust them according their resolution
			phaseAcc[0] += params.tuningWord[0];
			phaseAcc[0] &= (1 << PHASE_ACC_RES) - 1;
			phaseAcc[1] += params.tuningWord[1];
			phaseAcc[1] &= (1 << PHASE_ACC_RES) - 1;
			//In case a note is being pressed
			if(params.gate) {
				//If the envelope hasn't reached its maximum value
				if(envelope < (1 << ENVELOPE_RES) - 1) {
					//Increment the phase accumulator
					envPhaseAcc += params.attTuningWord;
				}
			} else if(envelope > 0) {
				//If no note is being pressed, and the envelope hasn't reached its
				//minimum value, decrement the phase accumulator
				envPhaseAcc -= params.relTuningWord;
			}
			//Re-adjust the phase accumulator according to its resolution
			envPhaseAcc &= (1 << PHASE_ACC_RES) - 1;
			//Only the MSB's in the phase accumulator are considered
			envelope = envPhaseAcc >> (PHASE_ACC_RES - ENVELOPE_RES);
			//Calculate the final sample as the sum of the individual samples
			//modulated by the envelope
			output = ((sample[0] + sample[1]) * envelope) >> 9;
		}
	}
}

//Initialize the timer
void initTimer(void)
{
	ConfigIntTimer1(T1_INT_ON | T1_INT_PRIOR_2);
	INTEnableSystemMultiVectoredInt();
	OpenTimer1(T1_ON | T1_SOURCE_INT | T1_PS_1_1, T1_TICK);
}

/*
void initTimer2(void)
{
	ConfigIntTimer2(T2_INT_ON | T2_INT_PRIOR_2);
	INTEnableSystemMultiVectoredInt();
	OpenTimer2(T2_ON | T2_SOURCE_INT | T2_PS_1_256, T2_TICK);
}
*/

//Initialize the ADC
void initADC(void)
{
	AD1PCFG = ~((1 << AN8) | (1 << AN5) | 
				(1 << AN4) | (1 << AN3) | (1 << AN2));
	AD1CON1 = (1 << SSRC2) | (1 << SSRC1) | (1 << SSRC0);
	AD1CON2 = 0;
	AD1CON3 = ADC_SHC_TIME;
	AD1CSSL = 0;
	AD1CHS = (AN2 << 16);
	AD1CON1SET = (1 << ON);
}

//Initialize the parameters to a default value
void initParams(struct Parameters *params)
{
	params->tuningWord[0] = 0;
	params->tuningWord[1] = 0;
	params->gate = 0;
	params->attTuningWord = 0;
	params->relTuningWord = 0;
	params->pwmValue = 128;
	params->detune = 0;
	params->wave = SINE;
}

//Read analog value on a given ADC channel
uint16_t acquireADC(uint8_t channel)
{
	AD1CHS = channel << 16;
	AD1CON1SET = (1 << SAMP);
	while(!(AD1CON1 & (1 << DONE)));
	return ADC1BUF0;
}

uint32_t calcSample(enum waveform wave, uint32_t phase, uint8_t pwmValue)
{
	//Only the MSB's in the phase accumulator are used for table lookup
	uint8_t phaseMSBs = phase >> (PHASE_ACC_RES - LOOKUP_RES);
	//Generate sample for appropriate waveform
	switch(wave) {
		case SINE:
			//Determine which quarter of the sine wave are we in
			//and calculate the sample accordingly
			switch(phaseMSBs >> (LOOKUP_RES - 2)) {
				case 0:
					return sineTable[phaseMSBs];
				case 1:
					return sineTable[128 - phaseMSBs];
				case 2:
					return 256 - sineTable[phaseMSBs - 128];
				case 3:
					return 256 - sineTable[256 - phaseMSBs];
			}
		case TRIANGLE:
			//Determine which half of the triangle wave are we in
			//and calculate the sample accordingly
			switch(phaseMSBs >> (LOOKUP_RES - 1)) {
				case 0:
					return 2 * phaseMSBs;
				case 1:
					return 510 - (2 * phaseMSBs);
			}
		case SAW:
			//Simply return the (scaled) value of the phase accumulator
			return phaseMSBs;
		case SQUARE:
			//Compare the phase accumulator's value with the threshold 
			//set by "pwmValue"
			return (phaseMSBs < pwmValue) * 0xFF;
	}
}

/*
void filterSample(float cutoff)
{
	sample = prevSample + (cutoff * (sample - prevSample));
	prevSample = sample;
}
*/

void readParams(struct Parameters *params)
{
	//Read the waveform select buttons
	if(!(BUTTON_PORT & (1 << SAW_BUTTON))) {
		params->wave = SAW;
	} else if(!(BUTTON_PORT & (1 << SQR_BUTTON))) {
		params->wave = SQUARE;
	} else if(!(BUTTON_PORT & (1 << TRI_BUTTON))) {
		params->wave = TRIANGLE;
	} else if(!(BUTTON_PORT & (1 << SIN_BUTTON))) {
		params->wave = SINE;
	}
	//Read the pulse width potentiometer
	params->pwmValue = (acquireADC(PW_POT) >> 2);
	//Read the attack and release potentiometers and scale the values to
	//an useful range
	params->attTuningWord = (acquireADC(ATTACK_POT) >> 1) + 10;
	params->relTuningWord = (acquireADC(RELEASE_POT) >> 1) + 10;
	//Read the detune potentiometer and zero-center the result
	params->detune = acquireADC(DETUNE_POT) - 512;
	//Read the keyboard only after a given number of interrupts have
	//passed since the last reading (to ensure proper debouncing)
	if(interruptCounter >= KBD_SAMPLE) {
		interruptCounter = 0;
		readKeyboard(params);
	}
}

void readKeyboard(struct Parameters *params)
{
	//Read the analog value on the corresponding pin
	uint16_t adcRead = acquireADC(KEYBOARD);
	//If there is a note being pressed
	if(adcRead > 25) {
		//Do some math to find the note being pressed
		params->tuningWord[0] = notes[(adcRead - 10) >> 5];
		//The second oscillator plays the same note but one octave lower
		//and with a small shift controllable by the detune potentiometer
		//to create a "beating" effect
		params->tuningWord[1] = (params->tuningWord[0] + params->detune) >> 1;
		//Turn on the gate flag
		params->gate = 1;
	} else {
		//Turn off the gate flag
		params->gate = 0;
	}
}

//Interrupt routine that fires at 40KHz
void __ISR(_TIMER_1_VECTOR, ipl2) Timer1Handler(void)
{
	//Clear the interrupt flag
	mT1ClearIntFlag();
	//Write the sample to the DAC
	LATE = output;
	//Variable used for keyboard debouncing
	++interruptCounter;
	phaseChange = 1;
}

/*
void __ISR(_TIMER_2_VECTOR, ipl2) Timer2Handler(void)
{
	mT2ClearIntFlag();
	static uint8_t i = 0;
	if(sequence[i] != 0) {
		gate = 1;
		tuningWord[0] = sequence[i];
		tuningWord[1] = (tuningWord[0] + detune) >> 1;
	} else {
		gate = 0;
	}
	if(i < SEQUENCE_LEN - 1) {
		++i;
	} else {
		i = 0;
	}
}
*/
