MCU   = 32MX320F128H

PROGRAMMER_TYPE = stk500v2
PROGRAMMER_ARGS = -C $(AVRDUDE_CONFIG) -P /dev/ttyUSB0

CHIPKIT_CONFIG = config/chipKIT-UNO32.ld
AVRDUDE_CONFIG = config/avrdude.conf

CC = xc32-gcc
LANG = c
OBJCOPY = xc32-bin2hex
OBJDUMP = xc32-objdump
AVRDUDE = avrdude
SIZE = xc32-size

TARGET = $(lastword $(subst /, ,$(CURDIR)))
SOURCES=$(wildcard *.c)
OBJECTS=$(SOURCES:.c=.o)
HEADERS=$(SOURCES:.c=.h)

CFLAGS = -g -x $(LANG) -MMD -MF $(TARGET).o.d
LDFLAGS = -Wl,--script=$(CHIPKIT_CONFIG),-Map=$(TARGET).map
TARGET_ARCH = -mprocessor=$(MCU)

.DEFAULT_GOAL := all

%.o: %.c $(HEADERS) Makefile
	 $(CC) $(CFLAGS) $(TARGET_ARCH) -c -o $@ $<;

$(TARGET).elf: $(OBJECTS)
	$(CC) $(TARGET_ARCH) $^ $(LDLIBS) -o $@ $(LDFLAGS) 

%.hex: %.elf
	$(OBJCOPY) $<

%.lst: %.elf
	$(OBJDUMP) -S $< > $@

.PHONY: all disasm size clean fclean flash

all: $(TARGET).hex

disasm: $(TARGET).lst

size: $(TARGET).elf
	$(SIZE) -C $(TARGET).elf

clean:
	rm -f $(TARGET).elf $(TARGET).hex $(TARGET).obj \
	$(TARGET).o $(TARGET).d $(TARGET).eep $(TARGET).lst \
	$(TARGET).lss $(TARGET).sym $(TARGET).map $(TARGET)~ \
	$(TARGET).eeprom

fclean:
	rm -f *.elf *.hex *.obj *.o *.d *.eep *.lst *.lss *.sym *.map *~ *.eeprom

flash: $(TARGET).hex
	$(AVRDUDE) -c $(PROGRAMMER_TYPE) -p $(MCU) $(PROGRAMMER_ARGS) \
	-U flash:w:$<
